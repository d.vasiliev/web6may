<?php
include 'database.php';
include 'funcs.php';
isAdmin($db);

//$id = $db->quote($_POST['id']);
$id = $_POST['id'];
$req = "DELETE FROM sposobnosti WHERE id=?";
$result = $db->prepare($req);
$result->execute(array($id));
$req = "DELETE FROM form WHERE id=?";
$result = $db->prepare($req);
$result->execute(array($id));

header('Location:admin.php');
?>
