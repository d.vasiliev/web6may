<?php
header('Content-Type: text/html; charset=UTF-8');
//define ('database',TRUE);
include 'database.php';
include 'funcs.php';
isAdmin($db);
//$id = $db->quote($_GET['id']);
$id = strip_tags($_POST['id']);
$stmt = $db->prepare("SELECT login FROM form WHERE id = ?");
$stmt->execute(array($id));
$user_login='';
while($row = $stmt->fetch())
{
    $user_login=$row['login'];
}

$request = "SELECT * FROM form where id=?";
$sth = $db->prepare($request);
$sth->execute(array($id));
$data = $sth->fetch(PDO::FETCH_ASSOC);
if($data==false){
    header('Location:admin.php');
    exit();
}
session_start();

$_SESSION['login'] = $user_login;
$_SESSION['uid'] = strip_tags($id);

setcookie('admin', '1');

header('Location: index.php');
?>
