<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css" media="all">    
    <title>Форма</title>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
.error-radio{
  border: 2px solid red;
  width: 200px;
        }
.error-checkbox {
  width: 400px;
  border: 2px solid red;      
        }
    </style>
</head>
<body>

<?php
if (!empty($messages)) {
    print('<div  ');
// Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}
?>
    
    <div>
    <form action="index.php" method="POST" id="form" accept-charset="UTF-8">
        <h1>Заполните анкету</h1>
<p><label> Имя <input type="text" name="fio" 
<?php if ($errors['fio']) {print 'class="error"';} ?>
       value="<?php print $values['fio']; ?>"> <br> </label></p>
    
    <p><label> E-mail <input type="email" name="email" 
      <?php if ($errors['email']) {print 'class="error"';} ?> 
     
      value="<?php print $values['email']; ?>"> <br> </label></p>
    <p><label> Дата рождения<input type="date" name="bdate" 
      <?php if($errors['bdate']) {print 'class = "error"';}?> 
      value="<?php print $values['bdate']; ?>" > <br></label></p>
    
    <label> Пол:
    <p><label <?php if($errors['sex']) { print 'class = "error-radio"';} ?>>
        <input type="radio" name="sex" value="male"
        <?php if($values['sex']=="male") {print 'checked';}?>/> Мужчина</label></p>
    <p><label <?php if($errors['sex']) { print 'class = "error-radio"';} ?>>
        <input type="radio" name="sex" value="female"
            <?php if($values['sex']=="female") {print 'checked';}?>/> Женщина</label></p>


    Количество конечностей:
    <p><label <?php if($errors['limb']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="1"
            <?php if($values['limb']=="1") {print 'checked';}?>/>
        1</label>
    <label <?php if($errors['limb']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="2"
            <?php if($values['limb']=="2") {print 'checked';}?>/>
        2</label>
    <label <?php if($errors['limb']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="3"
            <?php if($values['limb']=="3") {print 'checked';}?>/>
        3</label></p>
    <p><label <?php if($errors['limb']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="4"
            <?php if($values['limb']=="4") {print 'checked';}?>/>
        4</label>
    <label <?php if($errors['limb']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="5"
            <?php if($values['limb']=="5") {print 'checked';}?>/>
        5</label>
        <label <?php if($errors['limb']) { print 'class = "error-radio"';} ?>><input type="radio"
                  name="limb" value="6"
            <?php if($values['limb']=="6") {print 'checked';}?>/>
        6</label></p>
    <label>

      <p>Сверхспособности: <br>
      <select name="superpower[]" id="superpowers" multiple="multiple">
        <option value='godmode'>Бессмертие</option>
        <option value='wallhack'>Прохождение сквозь стены</option>
        <option value='noclip'>Левитация </option>
      </select>
    </label>
    <br>
</p>

    <p><label>
        Биография: <br/>
    <textarea name="bio"
        <?php if ($errors['bio']) 
        {print 'class="error"';} ?>
    >
        </textarea>
    </label>
    <br/>
    </p>



    <p><label <?php if($errors['check']) { print 'class = "error-checkbox"';} ?>>
       <input type="checkbox" checked="checked" name="checkbox" value="1"
       <?php if($values['check']=="1") {print 'checked';}?>>
       
       С условиями <a href="ftp://random.com/programs/contract.pdf"><em>контракта</em></a> ознакомлен.
    </label></p>

    <input type="submit" value="Отправить" id="submit"/>
    </form>
</div>
</body>
